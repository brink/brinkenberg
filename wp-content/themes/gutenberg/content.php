<?php
/**
 * @package gutenberg
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<header class="entry-header">
		<h1 class="entry-title">
			<a href="<?php the_permalink(); ?>" rel="bookmark">
				<?php the_title(); ?>
			</a>
		</h1>

		<?php if ( 'post' == get_post_type() ) : ?>
		<div class="entry-meta">
			<?php brinkenberg_posted_on(); ?>
		</div>
		<?php endif; ?>
	</header>

	<?php if ( !is_singular() ) : // only display excerpts for indexes ?>
	<div class="entry-summary">
		<?php the_excerpt(); ?>
		<a class="entry-more" href="<?php echo get_permalink(get_the_ID()); ?>">read more</a>
	</div>
	<?php else: ?>
	<div class="entry-content">
		<?php the_content(); ?>
	</div>
	<?php endif; ?>

</article>