<?php
/**
 * Template Name: Home
 *
 * @package gutenberg
 */

get_header(); ?>

<main class="site-content" role="main">

  <?php while ( have_posts() ) : the_post(); ?>

      <?php the_content(); ?>

  <?php endwhile; // end of the loop. ?>

</main>

<?php get_footer(); ?>