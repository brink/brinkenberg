<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content after
 *
 * @package gutenberg
 *
 * AS YOU ADD JAVASCRIPT files here, please consider adding them
 * to to the uglification process in Gruntfile.js.
 */

$footer_copy = get_field('footer_copy', 'option');
$copyright = get_field('copyright', 'option');
?>

    	</div>

    	<footer class="site-footer">
    		<div class="container">
    			<div class="footer-column">
	    			<?php if($footer_copy): ?>
		    			<?php echo $footer_copy; ?>
	    			<?php endif; ?>
	    			<?php if($copyright): ?>
		    		<div class="copyright">
		    			&copy; <?php echo date('Y'); ?> <?php echo $copyright; ?>
		    		</div>
		    		<?php endif; ?>
		    	</div>
    			<div class="footer-column">
		    		<?php wp_nav_menu(array('theme_location' => 'footer_menu')); ?>
		    	</div>
	    	</div>
    	</footer>

    </div>

    <?php wp_footer(); ?>

  </body>
</html>