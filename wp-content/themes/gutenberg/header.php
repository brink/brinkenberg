<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package gutenberg
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
	<head>
		<meta charset="<?php bloginfo('charset'); ?>">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- CHECK META CONTENT -->
		<title><?php wp_title( '|', true, 'right' ); ?></title>

		<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">

		<?php /* if you want to use modernizr
		<!-- Don't forget to replace Modernizr's full dev script with a custom build! -->
		<script src="<?php echo get_template_directory_uri(); ?>/public_assets/js/modernizr-dev.js"></script>
		*/ ?>
		<style>#skipNav{display: block;background-color:#fff;padding:1em;position: absolute;top: 0;left: -9999px;}#skipNav:focus{left:0;}</style>

		<?php wp_head(); ?>

		<?php /* we use yoast google analytics plugin, which auto generates this
		<script>
		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		  ga('create', 'UA-XXXXXXXX-XX', 'auto');
		  ga('send', 'pageview');
		</script>
		*/ ?>
	</head>

	<body <?php body_class(); ?>>

		<div id="page" class="page">

			<?php do_action('before'); ?>

			<header class="site-header" role="banner">

				<a class="site-title" href="<?php echo esc_url(home_url('/')); ?>" title="<?php echo esc_attr(get_bloginfo('name', 'display')); ?>" rel="home">
					<?php bloginfo('name'); ?>
				</a>

				<nav class="site-nav-c" role="navigation">
					<a id="skipNav" href="#siteBody">Skip navigation</a>
					<?php wp_nav_menu(array('theme_location' => 'primary')); ?>
				</nav>

				<div id="hamburger">
					<span></span>
					<span></span>
					<span></span>
				</div>

			</header>

			<div id="siteBody" class="site-body">