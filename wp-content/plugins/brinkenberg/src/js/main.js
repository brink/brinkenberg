var brinkenberg = {
	pos: 0,
    setCookie: function(name,value,days) {
        var expires = "";
        if (days) {
            var date = new Date();
            date.setTime(date.getTime() + (days*24*60*60*1000));
            expires = "; expires=" + date.toUTCString();
        }
        document.cookie = name + "=" + (value || "")  + expires + "; path=/";
    },
    getCookie: function(name) {
        var nameEQ = name + "=";
        var ca = document.cookie.split(';');
        for(var i=0;i < ca.length;i++) {
            var c = ca[i];
            while (c.charAt(0)==' ') c = c.substring(1,c.length);
            if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
        }
        return null;
    },
    eraseCookie: function(name) {   
        document.cookie = name+'=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/';  
    }    
}

jQuery(document).ready(function($){

	$(window).resize(function(){
		$('#hamburger').removeClass('active');
		$('.site-nav-c').removeClass('active');
	});

	$(window).scroll(function(){
		brinkenberg.pos = $(window).scrollTop();
	});

    $('#hamburger').click(function(){
        if($(this).hasClass('active'))
        {
            $(this).removeClass('active');
            $('.site-nav-c').removeClass('active');
        }
        else
        {
            $(this).addClass('active');
            $('.site-nav-c').addClass('active');
        }
    });

    $('a').click(function(e){
        var href = $(this).attr('href');
        var base = window.location.host;

        if(href.indexOf('http') > -1)
        {
            if(href.indexOf(base) < 0)
            {
                e.preventDefault();
                window.open(href, '_blank');
            }            
        }
    });    

	if($('.standard-slideshow').length)
	{
		function brinkenbergMoveSlide(parent, dir)
		{
			var next;
			var new_offset;
			var index = parseInt(parent.find('.standard-slide.active').data('index'));

			if(dir == 'left')
			{
				next = index - 1;
				if(next < 0)
				{
					next = parent.find('.standard-slide').length - 1;
				}

				parent.find('.standard-slide.slide-' + next).css({left: '-105%'});

				new_offset = '105%';
			}
			else
			{
				next = index + 1;

				if(next > parent.find('.standard-slide').length - 1)
				{
					next = 0;
				}

				parent.find('.standard-slide.slide-' + next).css({left: '105%'});

				new_offset = '-105%';
			}

			parent.find('.dot.active').removeClass('active');
			parent.find('.dot-' + next).addClass('active');

			parent.find('.standard-slide.slide-' + next).animate({left: 0}, 500, function(){
				$(this).addClass('active');
			});

			parent.find('.standard-slide.active').animate({left: new_offset}, 500, function(){
				$(this).removeClass('active');
				parent.removeClass('moving');
			});
		}

		$('.standard-slideshow .arrow').click(function(){
			var parent = $(this).parent();

			if(parent.hasClass('moving'))
			{
				return false;
			}

			parent.addClass('moving interacted');

			var dir = $(this).hasClass('left') ? 'left' : 'right';

			brinkenbergMoveSlide(parent, dir);
		});

		$('.standard-slideshow .dot').click(function(){
			var parent = $(this).parent().parent();

			if(parent.hasClass('moving') || $(this).hasClass('active'))
			{
				return false;
			}

			parent.addClass('moving interacted');

			var index = parseInt($(this).data('index'));
			var curindex = parseInt(parent.find('.standard-slide.active').data('index'));
			var dir;
			var new_offset;

			if(index > curindex)
			{
				dir = 'right';
				new_offset = '-105%';
				parent.find('.standard-slide.slide-' + index).css({left: '105%'});
			}
			else
			{
				dir = 'left';
				new_offset = '105%';
				parent.find('.standard-slide.slide-' + index).css({left: '-105%'});
			}

			parent.find('.dot.active').removeClass('active');
			parent.find('.dot-' + index).addClass('active');

			parent.find('.standard-slide.slide-' + index).animate({left: 0}, 500, function(){
				$(this).addClass('active');
			});

			parent.find('.standard-slide.active').animate({left: new_offset}, 500, function(){
				$(this).removeClass('active');
				parent.removeClass('moving');
			});
		});

		if($('.standard-slideshow.autoplay').length)
		{
			var pos = 0;
			var queue = [];
			var id;
			var delay = 5000;

			$(window).scroll(function(){
				pos = $(window).scrollTop();
				
				$('.standard-slideshow.autoplay').each(function(){
					if(pos > $(this).offset().top - ($(window).height() / 2) && !$(this).hasClass('playing'))
					{
						$(this).addClass('playing');
						id = $(this).attr('id');

						setTimeout(function(){
							queue.push(id);
						}, delay);
					}
				});
			});

			setInterval(function(){
				if(queue.length)
				{
					for(var i = 0; i < queue.length; i ++)
					{
						if(!$('#' + queue[i]).hasClass('interacted'))
						{
							brinkenbergMoveSlide($('#' + queue[i]), 'next');
						}
					}
				}
			}, delay);
		}
	}

	if($('.split-form').length)
	{
		$('.split-form .button').click(function(e){
			e.preventDefault();

			var parent = $(this).parent().parent().parent();

			if(parent.hasClass('processing'))
			{
				return false;
			}

			parent.find('.message').slideUp();
			parent.find('input').removeClass('error');

			var fname = parent.find('input[name="first-name"]').val();
			var lname = parent.find('input[name="last-name"]').val();
			var email = parent.find('input[name="email"]').val();
			var optin = false;

			if(parent.find('input[name="optin"]').length)
			{
				optin = parent.find('input[name="optin"]').is(':checked');
			}

			var errors = [];

			if($.trim(fname) == '')
			{
				errors.push('Please enter your first name.');
				parent.find('input[name="first-name"]').addClass('error');
			}

			if($.trim(lname) == '')
			{
				errors.push('Please enter your last name.');
				parent.find('input[name="last-name"]').addClass('error');
			}

			if($.trim(email) == '')
			{
				errors.push('Please enter your email address.');
				parent.find('input[name="email"]').addClass('error');
			}

			if(errors.length == 0)
			{
				//send to server
				$.ajax({
					type: "POST",
					dataType:"json",
					url: ajaxurl,
					data: { action: "mailing_list_form", fname: fname, lname:lname, email:email, optin:optin }
		 		})
		 		.done(function( msg ) {
		 			if(msg.status == "success")
		 			{
						parent.find('.message').removeClass('error').text(msg.message).slideDown();
						parent.find('.form-row').not('.message').slideUp();
		 			}
		 			else
		 			{
		 				parent.find('.message').addClass('error').html(msg.message).slideDown().delay(3000).slideUp();
		 			}
		 		});


			}
			else
			{
				parent.find('.message').addClass('error').html(errors.join('<br>')).slideDown().delay(3000).slideUp();
			}
		});

		$('.checkbox-styled').click(function(){
			if($(this).hasClass('active'))
			{
				$(this).removeClass('active');
				$(this).siblings('input[type="checkbox"]').prop('checked', false);
			}
			else
			{
				$(this).addClass('active');
				$(this).siblings('input[type="checkbox"]').prop('checked', true);
			}
		});
	}

	if($('.accordion').length)
	{
		$('.accordion-title').click(function(){
			if($(this).hasClass('opened'))
			{
				$(this).removeClass('opened');
				$(this).siblings('.accordion-text').slideUp();
			}
			else
			{
				$(this).addClass('opened');
				$(this).siblings('.accordion-text').slideDown();
			}
		});
	}
});