<?php
/**
 * @package BRINKenberg
 * @version 1.0
 */
/*
Plugin Name: BRINKenberg
Plugin URI: http://brink.com/
Description: default blocks for the gutenberg editor
Author: BRINKmedia, Inc
Version: 1.0
Author URI: http://brink.com
*/

/**
 * Enqueue scripts and styles
 */
function brink_scripts() {

	function the_scripts() {
		global $wp_scripts;
		// load third-party libs
		wp_enqueue_script('jquery');
		//wp_register_script( 'libs', plugin_dir_path('/public_assets/js/libs.js'), array(), false, true);
		//wp_enqueue_script( 'libs', plugins_url('/public_assets/js/libs.js', __FILE__ ));
		// load compiled app
		wp_enqueue_script( 'app', plugins_url('/public_assets/js/app.js', __FILE__ ));

	 	// load css
	 	wp_enqueue_style( 'css', plugins_url( '/public_assets/css/main.css', __FILE__ ), array(), null );
	}

	if (!is_admin()) 
		add_action("wp_enqueue_scripts", "the_scripts", 11);
	
}
add_action( 'wp_enqueue_scripts', 'brink_scripts' );

add_action('acf/init', 'brink_custom_blocks');

function brink_custom_blocks() {

    // a list of dash icons: https://www.kevinleary.net/wordpress-dashicons-list-custom-post-type-icons/

    if( function_exists('acf_register_block') ) {
        
        acf_register_block(array(
            'name'              => 'custom header',
            'title'             => __('Customized Header'),
            'description'       => __('Customize the top of the content section'),
            'render_callback'   => 'brink_block_render_callback',
            'category'          => 'common',
            'icon'              => 'admin-customizer',
            'mode'              => 'edit'
        ));
        
        acf_register_block(array(
            'name'              => 'standard text',
            'title'             => __('Standard Text'),
            'description'       => __('Just a standard WYSIWYG'),
            'render_callback'   => 'brink_block_render_callback',
            'category'          => 'common',
            'icon'              => 'admin-customizer',
            'mode'              => 'edit'
        ));
        
        acf_register_block(array(
            'name'              => 'feature box',
            'title'             => __('Feature Box'),
            'description'       => __('Standard appetizer block'),
            'render_callback'   => 'brink_block_render_callback',
            'category'          => 'common',
            'icon'              => 'screenoptions',
            'mode'              => 'edit'
        ));
        
        acf_register_block(array(
            'name'              => 'standard slideshow',
            'title'             => __('Standard Slideshow'),
            'description'       => __('Conventional slideshow with options'),
            'render_callback'   => 'brink_block_render_callback',
            'category'          => 'common',
            'icon'              => 'format-gallery',
            'mode'              => 'edit'
        ));
        
        acf_register_block(array(
            'name'              => 'mailing list form',
            'title'             => __('Mailing List Form'),
            'description'       => __('Standard split mailing list form'),
            'render_callback'   => 'brink_block_render_callback',
            'category'          => 'common',
            'icon'              => 'forms',
            'mode'              => 'edit'
        ));
        
        acf_register_block(array(
            'name'              => 'accordion',
            'title'             => __('Accordion'),
            'description'       => __('Accordion'),
            'render_callback'   => 'brink_block_render_callback',
            'category'          => 'common',
            'icon'              => 'list-view',
            'mode'              => 'edit'
        ));
    }
}

function brink_block_render_callback( $block ) {
    // convert name ("acf/testimonial") into path friendly slug ("testimonial")
    $slug = str_replace('acf/', '', $block['name']);

    // include a template part from within the "template-parts/block" folder
    if( file_exists(dirname(__FILE__)."/blocks/".$slug.".php") ) {
        
        $fw = get_field('full_width');

        if(!$fw) {echo "<div class='container'>";}
            echo "<div class='brink-custom-block block-" . $slug . "'>";
                    include( dirname(__FILE__)."/blocks/".$slug.".php" );
            echo "</div>";                    
        if(!$fw) { echo "</div>";}
        
    }
}

/* for adding custom post types
add_action( 'init', 'gutenberg_create_post_type' );
function gutenberg_create_post_type() {

	register_post_type( 'events',
		array(
			'labels' => array(
				'name' => __( 'Events' ),
				'singular_name' => __( 'Event' )
			),
			'public' => true,
			'has_archive' => false,
			'taxonomies' => array('post_tag', 'category'),
			//'rewrite' => array('slug' => 'exhibition'),
			'supports' => array(
				'title',
				'editor',
				'thumbnail'
			)
		)
	);
}
*/

/* for adding paginated pages on custom index types
add_action( 'init', 'gutenberg_add_rewrite' );
function gutenberg_add_rewrite()
{
	//flush_rewrite_rules();
    add_rewrite_rule(
        '^example/page/([0-9]+)/?$',
        'index.php?page_id=2&paged=$matches[1]',
        'top'
    );
}
*/

add_action("wp_ajax_mailing_list_form", "brink_mailing_list_form");
add_action("wp_ajax_nopriv_mailing_list_form", "brink_mailing_list_form");

function brink_mailing_list_form(){
    $fname = $_POST['fname'];
    $lname = $_POST['lname'];
    $email = $_POST['email'];
    $optin = $_POST['optin'];

    $errors = array();

    if($fname == '')
    {
        $errors[] = 'Please enter your first name.';
    }

    if($lname == '')
    {
        $errors[] = 'Please enter your last name.';
    }

    if($email == '' || !filter_var( $email ,FILTER_VALIDATE_EMAIL ))
    {
        $errors[] = 'Please enter a valid email address.';
    }

    if(count($errors) > 0)
    {
        $status = 'fail';
        $message = implode('<br>', $errors);
    }
    else
    {
        // integrate with some kind of CRM

        if($optin){
            // send to a mailing list or something
        }

        $status = 'success';
        $message = 'Thank you for signing up!';
    }

    echo json_encode(array('status' => $status, 'message' => $message));
    exit;
}

function brink_srcset($image, $alt, $class)
{
    $html = '<img class="'.($class ? $class : 'srcset').'" ';

    $html .= 'srcset="';

    if(isset($image['sizes']) && isset($image['sizes']['thumbnail']))
    {
        $html .= $image['sizes']['thumbnail'] . ' ' . $image['sizes']['thumbnail-width'] .'w,';
    }

    if(isset($image['sizes']) && isset($image['sizes']['medium']))
    {
        $html .= $image['sizes']['medium'] . ' ' . $image['sizes']['medium-width'] .'w,';
    }

    if(isset($image['sizes']) && isset($image['sizes']['medium_large']))
    {
        $html .= $image['sizes']['medium_large'] . ' ' . $image['sizes']['medium_large-width'] .'w,';
    }

    if(isset($image['sizes']) && isset($image['sizes']['large']))
    {
        $html .= $image['sizes']['large'] . ' ' . $image['sizes']['large-width'] .'w,';
    }

    if(isset($image['sizes']) && isset($image['sizes']['1536x1536']))
    {
        $html .= $image['sizes']['1536x1536'] . ' ' . $image['sizes']['1536x1536-width'] .'w,';
    }

    if(isset($image['sizes']) && isset($image['sizes']['2048x2048']))
    {
        $html .= $image['sizes']['2048x2048'] . ' ' . $image['sizes']['2048x2048-width'] .'w,';
    }

    $html .= '" ';

    $html .= 'sizes="';

    if(isset($image['sizes']) && isset($image['sizes']['thumbnail']))
    {
        $html .= 'max-width('.$image['sizes']['thumbnail-width'].'px) ' . (intval($image['sizes']['thumbnail-width']) - 40) .'px,';
    }

    if(isset($image['sizes']) && isset($image['sizes']['medium']))
    {
        $html .= 'max-width('.$image['sizes']['medium-width'].'px) ' . (intval($image['sizes']['medium-width']) - 40) .'px,';
    }

    if(isset($image['sizes']) && isset($image['sizes']['medium_large']))
    {
        $html .= 'max-width('.$image['sizes']['medium_large-width'].'px) ' . (intval($image['sizes']['medium_large-width']) - 40) .'px,';
    }

    if(isset($image['sizes']) && isset($image['sizes']['large']))
    {
        $html .= 'max-width('.$image['sizes']['large-width'].'px) ' . (intval($image['sizes']['large-width']) - 40) .'px,';
    }

    if(isset($image['sizes']) && isset($image['sizes']['1536x1536']))
    {
        $html .= 'max-width('.$image['sizes']['1536x1536-width'].'px) ' . (intval($image['sizes']['1536x1536-width']) - 40) .'px,';
    }

    if(isset($image['sizes']) && isset($image['sizes']['2048x2048']))
    {
        $html .= 'max-width('.$image['sizes']['2048x2048-width'].'px) ' . (intval($image['sizes']['2048x2048-width']) - 40) .'px,';
    }

    $html .= '" ';

    $html .= 'src="'.$image['url'].'" ';

    if($alt)
    {
        $html .= 'alt="'.str_replace('"', "'", $alt).'"';       
    }
    else
    {
        $html .= 'alt="image"';
    }

    $html .= '>'; 

    return $html;
}

function gutenberg_truncate($text, $chars = 25) {
    $text = $text." ";
    $text = substr($text,0,$chars);
    $text = substr($text,0,strrpos($text,' '));
    return $text;
}

add_action(
    'admin_head',
    function() {
        echo '<style>.wp-block{max-width: calc(100% - 4rem);}</style>';
    }
);

function brinkenberg_setup() {
    add_theme_support( 'align-wide' );
}
add_action( 'after_setup_theme', 'brinkenberg_setup' );

add_action('wp_head','brinkenberg_ajaxurl');
function brinkenberg_ajaxurl() {
?>
<script type="text/javascript">
var ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';
</script>
<?php
}

function yoasttobottom() {
	return 'low';
}
add_filter( 'wpseo_metabox_prio', 'yoasttobottom');

function brinkenberg_disable_custom_colors() {
    add_theme_support( 'disable-custom-colors' );
    add_theme_support( 'editor-color-palette', array(
    ) );
}
add_action( 'after_setup_theme', 'brinkenberg_disable_custom_colors' );

add_action('after_setup_theme', 'wpse_remove_custom_colors');
function wpse_remove_custom_colors() {
    // removes the text box where users can enter custom pixel sizes
    add_theme_support('disable-custom-font-sizes');
    // forces the dropdown for font sizes to only contain "normal"
    add_theme_support('editor-font-sizes', array(
        array(
            'name' => 'Normal',
            'slug' => 'normal'
        )
    ) );
}

function brinkenberg_enqueue() {
    wp_enqueue_script(
        'brinkenberg-script',
        plugins_url( 'brinkenberg.js', __FILE__ ),
        array(
            'wp-blocks',
            'wp-element',
            'wp-editor', // Note the addition of wp-editor to the dependencies
            'wp-dom-ready', 
            'wp-edit-post'    
        )
    );
}
add_action( 'enqueue_block_editor_assets', 'brinkenberg_enqueue' );

/*==========================
DEFAULT BLOCKS
==========================*/

function remove_default_blocks($allowed_blocks){
    // Get widget blocks and registered by plugins blocks
    $registered_blocks = WP_Block_Type_Registry::get_instance()->get_all_registered();

    // Disable Widgets Blocks
    unset($registered_blocks['core/calendar']);
    unset($registered_blocks['core/rss']);
    unset($registered_blocks['core/search']);
    unset($registered_blocks['core/tag-cloud']);
    unset($registered_blocks['core/latest-comments']);
    unset($registered_blocks['core/archives']);
    unset($registered_blocks['core/categories']);
    unset($registered_blocks['core/latest-posts']);
    unset($registered_blocks['core/shortcode']);

    // Disable WooCommerce Blocks
    unset($registered_blocks['woocommerce/handpicked-products']);
    unset($registered_blocks['woocommerce/product-best-sellers']);
    unset($registered_blocks['woocommerce/product-category']);
    unset($registered_blocks['woocommerce/product-new']);
    unset($registered_blocks['woocommerce/product-on-sale']);
    unset($registered_blocks['woocommerce/product-top-rated']);
    unset($registered_blocks['woocommerce/products-by-attribute']);
    unset($registered_blocks['woocommerce/featured-product']);

    // Now $registered_blocks contains only blocks registered by plugins, but we need keys only
    $registered_blocks = array_keys($registered_blocks);

    // Merge allowed core blocks with plugins blocks
    return array_merge(array(
        'core/group',
        'core/button',
        'core/spacer',
        'core/paragraph',
        'core/list',
        'core/heading',
        'core/image',
        'core/columns',
        'core/separator',
        'core/media-text',
        'core/shortcode',
        'core/video',
        'core/quote',
        'core/image',
        'core-embed/youtube',
        'core-embed/vimeo',
        'core-embed/twitter',
        'core-embed/facebook',
        'core-embed/instagram'
    ), $registered_blocks);
}

add_filter('allowed_block_types', 'remove_default_blocks');

if( function_exists('acf_add_options_page') ) {

    $parent = acf_add_options_page(array(
        'page_title'    => 'Theme Settings',
        'menu_title'    => 'Theme Settings',
        'menu_slug'     => 'theme-settings',
        'capability'    => 'edit_posts',
        'redirect'      => false,
        'icon_url' => 'data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIzMS41MTEiIGhlaWdodD0iNDcuMDgxIiB2aWV3Qm94PSIwIDAgMzEuNTExIDQ3LjA4MSI+ICA8ZyBpZD0iY2FjdHVzLWZhdmljb24iIHRyYW5zZm9ybT0idHJhbnNsYXRlKC0xLjgyOCAtMS44MjgpIj4gICAgPGcgaWQ9Ikdyb3VwXzk0NSIgZGF0YS1uYW1lPSJHcm91cCA5NDUiIHRyYW5zZm9ybT0idHJhbnNsYXRlKDEuODI4IDEuODI4KSI+ICAgICAgPGcgaWQ9Ikdyb3VwXzk0MyIgZGF0YS1uYW1lPSJHcm91cCA5NDMiIHRyYW5zZm9ybT0idHJhbnNsYXRlKDAgNy4zODQpIj4gICAgICAgIDxnIGlkPSJHcm91cF85NDAiIGRhdGEtbmFtZT0iR3JvdXAgOTQwIj4gICAgICAgICAgPGcgaWQ9Ikdyb3VwXzkzOSIgZGF0YS1uYW1lPSJHcm91cCA5MzkiPiAgICAgICAgICAgIDxwYXRoIGlkPSJQYXRoXzIzOCIgZGF0YS1uYW1lPSJQYXRoIDIzOCIgZD0iTTYuOTYsMzIuNTU4QTQuNDMxLDQuNDMxLDAsMCwxLDIuNSwyOC4xVjE3LjA2YTQuNDYsNC40NiwwLDAsMSw4LjkxOSwwVjI4LjFBNC4zODYsNC4zODYsMCwwLDEsNi45NiwzMi41NThaIiB0cmFuc2Zvcm09InRyYW5zbGF0ZSgtMi41IC0xMi42KSIgZmlsbD0iI2ZmZiIvPiAgICAgICAgICA8L2c+ICAgICAgICA8L2c+ICAgICAgICA8ZyBpZD0iR3JvdXBfOTQyIiBkYXRhLW5hbWU9Ikdyb3VwIDk0MiIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMjIuNTkgMTEuOTE3KSI+ICAgICAgICAgIDxnIGlkPSJHcm91cF85NDEiIGRhdGEtbmFtZT0iR3JvdXAgOTQxIj4gICAgICAgICAgICA8cGF0aCBpZD0iUGF0aF8yMzkiIGRhdGEtbmFtZT0iUGF0aCAyMzkiIGQ9Ik0zNy44Niw0Ni4yMjZhNC40MzEsNC40MzEsMCwwLDEtNC40Ni00LjQ2VjMzLjM2YTQuNDYsNC40NiwwLDEsMSw4LjkxOSwwdjguNDA3QTQuMzg2LDQuMzg2LDAsMCwxLDM3Ljg2LDQ2LjIyNloiIHRyYW5zZm9ybT0idHJhbnNsYXRlKC0zMy40IC0yOC45KSIgZmlsbD0iI2ZmZiIvPiAgICAgICAgICA8L2c+ICAgICAgICA8L2c+ICAgICAgPC9nPiAgICAgIDxnIGlkPSJHcm91cF85NDQiIGRhdGEtbmFtZT0iR3JvdXAgOTQ0IiB0cmFuc2Zvcm09InRyYW5zbGF0ZSgxMS4zMzIpIj4gICAgICAgIDxwYXRoIGlkPSJQYXRoXzI0MCIgZGF0YS1uYW1lPSJQYXRoIDI0MCIgZD0iTTI2LjkxOSw0OS41ODFoMFY2Ljk2QTQuNDYsNC40NiwwLDAsMCwxOCw2Ljk2VjQ5LjUwOGg4LjkxOVoiIHRyYW5zZm9ybT0idHJhbnNsYXRlKC0xOCAtMi41KSIgZmlsbD0iI2ZmZiIvPiAgICAgIDwvZz4gICAgPC9nPiAgPC9nPjwvc3ZnPg=='
    ));

    acf_add_options_sub_page(array(
        'page_title'    => 'Footer Content',
        'menu_title'    => 'Footer',
        'parent_slug'   => $parent['menu_slug'],
    ));

}

if (function_exists('register_nav_menu'))
{
    register_nav_menu('footer_menu', 'Footer Menu');
}