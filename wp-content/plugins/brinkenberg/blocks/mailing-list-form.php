<?php

/**
 * Block Name: Mailing List Form
 *
 * Split form. Lead in text with a conventional form, plus handling options
 */

?>

<?php

$title = get_field('title');
$copy = get_field('copy');
$optin = get_field('optin');
?>

<div class="split-form">
	<div class="container">
		<div class="split-form-cell">
			<h2><?php echo $title; ?></h2>
			<?php echo $copy; ?>
		</div>
		<div class="split-form-cell">
			<form id="split-form-<?php echo rand(100, 10000); ?>" method="post" action="" class="lead-form">
				<fieldset>
					<div class="form-row">
						<label for="first-name">First Name</label>
						<input type="text" name="first-name" placeholder="First Name">
					</div>
					<div class="form-row">
						<label for="last-name">Last Name</label>
						<input type="text" name="last-name" placeholder="Last Name">
					</div>
					<div class="form-row">
						<label for="email">Email</label>
						<input type="text" name="email" placeholder="Email">
					</div>
					<?php if($optin != ''): ?>
					<div class="form-row">
						<div class="checkbox-wrapper">
							<span class="checkbox-styled"></span>
							<input type="checkbox" name="optin" value="1">
							<?php echo $optin; ?>
						</div>
					</div>
					<?php endif; ?>
					<div class="form-row message"></div>
					<div class="form-row">
						<input type="hidden" name="list_id" value="xxx">
						<button class="form-submit button">Submit</button>
					</div>
				</fieldset>
			</form>
		</div>
	</div>
</div>