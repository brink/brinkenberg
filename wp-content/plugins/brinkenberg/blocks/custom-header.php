<?php

/**
 * Block Name: Custom Header
 *
 * This is the template that displays the team grid block
 */

?>

<?php

$title = get_field('title');
$subtitle = get_field('subtitle');
$image = get_field('image');
?>

<div class="page-header">
	<div class="page-header-container">
		<h1><?php echo $title; ?></h1>
		<div class="header-subtitle"><?php echo $subtitle; ?></div>
	</div>
	<?php echo brink_srcset($image, $title, 'hero-background'); ?>
</div>