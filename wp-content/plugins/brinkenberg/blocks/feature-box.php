<?php

/**
 * Block Name: Feature Box
 *
 * Individual featured boxes that can be embedded in columns or wherever you like
 */

?>

<?php
$type = get_field('type');

if($type == 'custom'){
	$title = get_field('title');
	$image = get_field('image');
	if($image)
	{
		$image = brink_srcset($image, $title, 'feature-box-image');
	}
	$copy = get_field('copy');
	$link_url = get_field('link_url');
	$cta_label = get_field('cta_label');
}
else
{
	$content = get_field('content');
	$post_id = get_field('post');
	$post = get_post($post_id);
	$title = $post->post_title;
	$image = get_the_post_thumbnail_url($post_id,'full');
	if($image)
	{
		$image = '<img src="'.$image.'" alt="'.$title.'">';
	}

	$copy = gutenberg_truncate($post->post_content, 45);
	$cta_label = 'Read More';
	$link_url = get_permalink($post_id);
}
?>
<div class="feature-box">
	<?php if($image){ echo $image; } ?>
	<h3><?php echo $title; ?></h3>
	<?php echo $copy; ?>
	<?php if($link_url != ''): ?>
	<a href="<?php echo $link_url; ?>" class="button"><?php echo $cta_label; ?></a>
	<a href="<?php echo $link_url; ?>" class="overlink"></a>
	<?php endif; ?>
</div>