<?php

/**
 * Block Name: Standard Text
 *
 * This is nothing more than a standard wysiwyg editor
 */

?>

<?php

$content = get_field('content');
?>

<div class="standard-text">
	<?php echo $content; ?>
</div>