<?php

/**
 * Block Name: Accordion
 *
 * This is the template that displays an accordion block
 */

$items = get_field('items');
if($items):
?>
<div class="accordion">
	<?php foreach($items AS $item): ?>
	<div class="accordion-item">
		<div class="accordion-title">
			<?php echo $item['lead_in_text']; ?>
		</div>
		<div class="accordion-text">
			<?php echo $item['drop_down_text']; ?>
		</div>
	</div>
	<?php endforeach; ?>
</div>
<?php endif; ?>