<?php

/**
 * Block Name: Standard Slideshow
 *
 * Your everyday slideshow with a few little controls
 */

?>

<?php

$slides = get_field('slides');
$arrows = get_field('show_navigation_arrows');
$dots = get_field('show_pagination_dots');
$autoplay = get_field('autoplay');

?>

<div id="standard-slideshow-<?php echo rand(1000, 10000); ?>" class="standard-slideshow <?php echo $autoplay ? 'autoplay' : ''; ?>">
	<div class="standard-slideshow-container">
		<?php
		$i = 0;
		foreach($slides AS $slide):
			//print_r($slide['image']);
		?>
		<div class="standard-slide <?php echo $i == 0 ? 'active' : ''; ?> slide-<?php echo $i; ?>" style="background-image:url(<?php echo $slide['image']['url']; ?>);" data-index="<?php echo $i; ?>">
			<div class="standard-slide-container">
				<h2><?php echo $slide['title']; ?></h2>
				<?php if($slide['subtitle']): ?>
				<h3><?php echo $slide['subtitle']; ?></h3>
				<?php endif; ?>
				<?php if($slide['text']): ?>
				<div class="standard-copy">
					<?php echo $slide['text']; ?>
				</div>
				<?php endif; ?>
				<?php if($slide['link_url']): ?>
				<a href="<?php echo $slide['link_url']; ?>" class="button"><?php echo $slide['cta_label']; ?></a>
				<?php endif; ?>
			</div>
		</div>
		<?php $i ++; endforeach; ?>
	</div>

	<span class="arrow left <?php echo $arrows ? '' : 'disabled'; ?>"></span>
	<span class="arrow right <?php echo $arrows ? '' : 'disabled'; ?>"></span>

	<?php if($dots): ?>
	<ul class="standard-dots">
		<?php
		for($i = 0; $i < count($slides); $i ++):
		?>
		<li class="dot <?php echo $i == 0 ? 'active' : ''; ?> dot-<?php echo $i; ?>" data-index="<?php echo $i; ?>"></li>
		<?php endfor; ?>
	</ul>
	<?php endif; ?>
</div>