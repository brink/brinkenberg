(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);throw new Error("Cannot find module '"+o+"'")}var f=n[o]={exports:{}};t[o][0].call(f.exports,function(e){var n=t[o][1][e];return s(n?n:e)},f,f.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
var brinkenberg = {
	pos: 0,
    setCookie: function(name,value,days) {
        var expires = "";
        if (days) {
            var date = new Date();
            date.setTime(date.getTime() + (days*24*60*60*1000));
            expires = "; expires=" + date.toUTCString();
        }
        document.cookie = name + "=" + (value || "")  + expires + "; path=/";
    },
    getCookie: function(name) {
        var nameEQ = name + "=";
        var ca = document.cookie.split(';');
        for(var i=0;i < ca.length;i++) {
            var c = ca[i];
            while (c.charAt(0)==' ') c = c.substring(1,c.length);
            if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
        }
        return null;
    },
    eraseCookie: function(name) {   
        document.cookie = name+'=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/';  
    }    
}

jQuery(document).ready(function($){

	$(window).resize(function(){
		$('#hamburger').removeClass('active');
		$('.site-nav-c').removeClass('active');
	});

	$(window).scroll(function(){
		brinkenberg.pos = $(window).scrollTop();
	});

    $('#hamburger').click(function(){
        if($(this).hasClass('active'))
        {
            $(this).removeClass('active');
            $('.site-nav-c').removeClass('active');
        }
        else
        {
            $(this).addClass('active');
            $('.site-nav-c').addClass('active');
        }
    });

    $('a').click(function(e){
        var href = $(this).attr('href');
        var base = window.location.host;

        if(href.indexOf('http') > -1)
        {
            if(href.indexOf(base) < 0)
            {
                e.preventDefault();
                window.open(href, '_blank');
            }            
        }
    });    

	if($('.standard-slideshow').length)
	{
		function brinkenbergMoveSlide(parent, dir)
		{
			var next;
			var new_offset;
			var index = parseInt(parent.find('.standard-slide.active').data('index'));

			if(dir == 'left')
			{
				next = index - 1;
				if(next < 0)
				{
					next = parent.find('.standard-slide').length - 1;
				}

				parent.find('.standard-slide.slide-' + next).css({left: '-105%'});

				new_offset = '105%';
			}
			else
			{
				next = index + 1;

				if(next > parent.find('.standard-slide').length - 1)
				{
					next = 0;
				}

				parent.find('.standard-slide.slide-' + next).css({left: '105%'});

				new_offset = '-105%';
			}

			parent.find('.dot.active').removeClass('active');
			parent.find('.dot-' + next).addClass('active');

			parent.find('.standard-slide.slide-' + next).animate({left: 0}, 500, function(){
				$(this).addClass('active');
			});

			parent.find('.standard-slide.active').animate({left: new_offset}, 500, function(){
				$(this).removeClass('active');
				parent.removeClass('moving');
			});
		}

		$('.standard-slideshow .arrow').click(function(){
			var parent = $(this).parent();

			if(parent.hasClass('moving'))
			{
				return false;
			}

			parent.addClass('moving interacted');

			var dir = $(this).hasClass('left') ? 'left' : 'right';

			brinkenbergMoveSlide(parent, dir);
		});

		$('.standard-slideshow .dot').click(function(){
			var parent = $(this).parent().parent();

			if(parent.hasClass('moving') || $(this).hasClass('active'))
			{
				return false;
			}

			parent.addClass('moving interacted');

			var index = parseInt($(this).data('index'));
			var curindex = parseInt(parent.find('.standard-slide.active').data('index'));
			var dir;
			var new_offset;

			if(index > curindex)
			{
				dir = 'right';
				new_offset = '-105%';
				parent.find('.standard-slide.slide-' + index).css({left: '105%'});
			}
			else
			{
				dir = 'left';
				new_offset = '105%';
				parent.find('.standard-slide.slide-' + index).css({left: '-105%'});
			}

			parent.find('.dot.active').removeClass('active');
			parent.find('.dot-' + index).addClass('active');

			parent.find('.standard-slide.slide-' + index).animate({left: 0}, 500, function(){
				$(this).addClass('active');
			});

			parent.find('.standard-slide.active').animate({left: new_offset}, 500, function(){
				$(this).removeClass('active');
				parent.removeClass('moving');
			});
		});

		if($('.standard-slideshow.autoplay').length)
		{
			var pos = 0;
			var queue = [];
			var id;
			var delay = 5000;

			$(window).scroll(function(){
				pos = $(window).scrollTop();
				
				$('.standard-slideshow.autoplay').each(function(){
					if(pos > $(this).offset().top - ($(window).height() / 2) && !$(this).hasClass('playing'))
					{
						$(this).addClass('playing');
						id = $(this).attr('id');

						setTimeout(function(){
							queue.push(id);
						}, delay);
					}
				});
			});

			setInterval(function(){
				if(queue.length)
				{
					for(var i = 0; i < queue.length; i ++)
					{
						if(!$('#' + queue[i]).hasClass('interacted'))
						{
							brinkenbergMoveSlide($('#' + queue[i]), 'next');
						}
					}
				}
			}, delay);
		}
	}

	if($('.split-form').length)
	{
		$('.split-form .button').click(function(e){
			e.preventDefault();

			var parent = $(this).parent().parent().parent();

			if(parent.hasClass('processing'))
			{
				return false;
			}

			parent.find('.message').slideUp();
			parent.find('input').removeClass('error');

			var fname = parent.find('input[name="first-name"]').val();
			var lname = parent.find('input[name="last-name"]').val();
			var email = parent.find('input[name="email"]').val();
			var optin = false;

			if(parent.find('input[name="optin"]').length)
			{
				optin = parent.find('input[name="optin"]').is(':checked');
			}

			var errors = [];

			if($.trim(fname) == '')
			{
				errors.push('Please enter your first name.');
				parent.find('input[name="first-name"]').addClass('error');
			}

			if($.trim(lname) == '')
			{
				errors.push('Please enter your last name.');
				parent.find('input[name="last-name"]').addClass('error');
			}

			if($.trim(email) == '')
			{
				errors.push('Please enter your email address.');
				parent.find('input[name="email"]').addClass('error');
			}

			if(errors.length == 0)
			{
				//send to server
				$.ajax({
					type: "POST",
					dataType:"json",
					url: ajaxurl,
					data: { action: "mailing_list_form", fname: fname, lname:lname, email:email, optin:optin }
		 		})
		 		.done(function( msg ) {
		 			if(msg.status == "success")
		 			{
						parent.find('.message').removeClass('error').text(msg.message).slideDown();
						parent.find('.form-row').not('.message').slideUp();
		 			}
		 			else
		 			{
		 				parent.find('.message').addClass('error').html(msg.message).slideDown().delay(3000).slideUp();
		 			}
		 		});


			}
			else
			{
				parent.find('.message').addClass('error').html(errors.join('<br>')).slideDown().delay(3000).slideUp();
			}
		});

		$('.checkbox-styled').click(function(){
			if($(this).hasClass('active'))
			{
				$(this).removeClass('active');
				$(this).siblings('input[type="checkbox"]').prop('checked', false);
			}
			else
			{
				$(this).addClass('active');
				$(this).siblings('input[type="checkbox"]').prop('checked', true);
			}
		});
	}

	if($('.accordion').length)
	{
		$('.accordion-title').click(function(){
			if($(this).hasClass('opened'))
			{
				$(this).removeClass('opened');
				$(this).siblings('.accordion-text').slideUp();
			}
			else
			{
				$(this).addClass('opened');
				$(this).siblings('.accordion-text').slideDown();
			}
		});
	}
});
},{}]},{},[1])
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9BcHBsaWNhdGlvbnMvTUFNUC9odGRvY3MvYnJpbmtlbmJlcmcvd3AtY29udGVudC9wbHVnaW5zL2JyaW5rZW5iZXJnL25vZGVfbW9kdWxlcy9icm93c2VyLXBhY2svX3ByZWx1ZGUuanMiLCIvQXBwbGljYXRpb25zL01BTVAvaHRkb2NzL2JyaW5rZW5iZXJnL3dwLWNvbnRlbnQvcGx1Z2lucy9icmlua2VuYmVyZy9zcmMvanMvbWFpbi5qcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtBQ0FBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSIsImZpbGUiOiJnZW5lcmF0ZWQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlc0NvbnRlbnQiOlsiKGZ1bmN0aW9uIGUodCxuLHIpe2Z1bmN0aW9uIHMobyx1KXtpZighbltvXSl7aWYoIXRbb10pe3ZhciBhPXR5cGVvZiByZXF1aXJlPT1cImZ1bmN0aW9uXCImJnJlcXVpcmU7aWYoIXUmJmEpcmV0dXJuIGEobywhMCk7aWYoaSlyZXR1cm4gaShvLCEwKTt0aHJvdyBuZXcgRXJyb3IoXCJDYW5ub3QgZmluZCBtb2R1bGUgJ1wiK28rXCInXCIpfXZhciBmPW5bb109e2V4cG9ydHM6e319O3Rbb11bMF0uY2FsbChmLmV4cG9ydHMsZnVuY3Rpb24oZSl7dmFyIG49dFtvXVsxXVtlXTtyZXR1cm4gcyhuP246ZSl9LGYsZi5leHBvcnRzLGUsdCxuLHIpfXJldHVybiBuW29dLmV4cG9ydHN9dmFyIGk9dHlwZW9mIHJlcXVpcmU9PVwiZnVuY3Rpb25cIiYmcmVxdWlyZTtmb3IodmFyIG89MDtvPHIubGVuZ3RoO28rKylzKHJbb10pO3JldHVybiBzfSkiLCJ2YXIgYnJpbmtlbmJlcmcgPSB7XG5cdHBvczogMCxcbiAgICBzZXRDb29raWU6IGZ1bmN0aW9uKG5hbWUsdmFsdWUsZGF5cykge1xuICAgICAgICB2YXIgZXhwaXJlcyA9IFwiXCI7XG4gICAgICAgIGlmIChkYXlzKSB7XG4gICAgICAgICAgICB2YXIgZGF0ZSA9IG5ldyBEYXRlKCk7XG4gICAgICAgICAgICBkYXRlLnNldFRpbWUoZGF0ZS5nZXRUaW1lKCkgKyAoZGF5cyoyNCo2MCo2MCoxMDAwKSk7XG4gICAgICAgICAgICBleHBpcmVzID0gXCI7IGV4cGlyZXM9XCIgKyBkYXRlLnRvVVRDU3RyaW5nKCk7XG4gICAgICAgIH1cbiAgICAgICAgZG9jdW1lbnQuY29va2llID0gbmFtZSArIFwiPVwiICsgKHZhbHVlIHx8IFwiXCIpICArIGV4cGlyZXMgKyBcIjsgcGF0aD0vXCI7XG4gICAgfSxcbiAgICBnZXRDb29raWU6IGZ1bmN0aW9uKG5hbWUpIHtcbiAgICAgICAgdmFyIG5hbWVFUSA9IG5hbWUgKyBcIj1cIjtcbiAgICAgICAgdmFyIGNhID0gZG9jdW1lbnQuY29va2llLnNwbGl0KCc7Jyk7XG4gICAgICAgIGZvcih2YXIgaT0wO2kgPCBjYS5sZW5ndGg7aSsrKSB7XG4gICAgICAgICAgICB2YXIgYyA9IGNhW2ldO1xuICAgICAgICAgICAgd2hpbGUgKGMuY2hhckF0KDApPT0nICcpIGMgPSBjLnN1YnN0cmluZygxLGMubGVuZ3RoKTtcbiAgICAgICAgICAgIGlmIChjLmluZGV4T2YobmFtZUVRKSA9PSAwKSByZXR1cm4gYy5zdWJzdHJpbmcobmFtZUVRLmxlbmd0aCxjLmxlbmd0aCk7XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIG51bGw7XG4gICAgfSxcbiAgICBlcmFzZUNvb2tpZTogZnVuY3Rpb24obmFtZSkgeyAgIFxuICAgICAgICBkb2N1bWVudC5jb29raWUgPSBuYW1lKyc9OyBleHBpcmVzPVRodSwgMDEgSmFuIDE5NzAgMDA6MDA6MDAgVVRDOyBwYXRoPS8nOyAgXG4gICAgfSAgICBcbn1cblxualF1ZXJ5KGRvY3VtZW50KS5yZWFkeShmdW5jdGlvbigkKXtcblxuXHQkKHdpbmRvdykucmVzaXplKGZ1bmN0aW9uKCl7XG5cdFx0JCgnI2hhbWJ1cmdlcicpLnJlbW92ZUNsYXNzKCdhY3RpdmUnKTtcblx0XHQkKCcuc2l0ZS1uYXYtYycpLnJlbW92ZUNsYXNzKCdhY3RpdmUnKTtcblx0fSk7XG5cblx0JCh3aW5kb3cpLnNjcm9sbChmdW5jdGlvbigpe1xuXHRcdGJyaW5rZW5iZXJnLnBvcyA9ICQod2luZG93KS5zY3JvbGxUb3AoKTtcblx0fSk7XG5cbiAgICAkKCcjaGFtYnVyZ2VyJykuY2xpY2soZnVuY3Rpb24oKXtcbiAgICAgICAgaWYoJCh0aGlzKS5oYXNDbGFzcygnYWN0aXZlJykpXG4gICAgICAgIHtcbiAgICAgICAgICAgICQodGhpcykucmVtb3ZlQ2xhc3MoJ2FjdGl2ZScpO1xuICAgICAgICAgICAgJCgnLnNpdGUtbmF2LWMnKS5yZW1vdmVDbGFzcygnYWN0aXZlJyk7XG4gICAgICAgIH1cbiAgICAgICAgZWxzZVxuICAgICAgICB7XG4gICAgICAgICAgICAkKHRoaXMpLmFkZENsYXNzKCdhY3RpdmUnKTtcbiAgICAgICAgICAgICQoJy5zaXRlLW5hdi1jJykuYWRkQ2xhc3MoJ2FjdGl2ZScpO1xuICAgICAgICB9XG4gICAgfSk7XG5cbiAgICAkKCdhJykuY2xpY2soZnVuY3Rpb24oZSl7XG4gICAgICAgIHZhciBocmVmID0gJCh0aGlzKS5hdHRyKCdocmVmJyk7XG4gICAgICAgIHZhciBiYXNlID0gd2luZG93LmxvY2F0aW9uLmhvc3Q7XG5cbiAgICAgICAgaWYoaHJlZi5pbmRleE9mKCdodHRwJykgPiAtMSlcbiAgICAgICAge1xuICAgICAgICAgICAgaWYoaHJlZi5pbmRleE9mKGJhc2UpIDwgMClcbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBlLnByZXZlbnREZWZhdWx0KCk7XG4gICAgICAgICAgICAgICAgd2luZG93Lm9wZW4oaHJlZiwgJ19ibGFuaycpO1xuICAgICAgICAgICAgfSAgICAgICAgICAgIFxuICAgICAgICB9XG4gICAgfSk7ICAgIFxuXG5cdGlmKCQoJy5zdGFuZGFyZC1zbGlkZXNob3cnKS5sZW5ndGgpXG5cdHtcblx0XHRmdW5jdGlvbiBicmlua2VuYmVyZ01vdmVTbGlkZShwYXJlbnQsIGRpcilcblx0XHR7XG5cdFx0XHR2YXIgbmV4dDtcblx0XHRcdHZhciBuZXdfb2Zmc2V0O1xuXHRcdFx0dmFyIGluZGV4ID0gcGFyc2VJbnQocGFyZW50LmZpbmQoJy5zdGFuZGFyZC1zbGlkZS5hY3RpdmUnKS5kYXRhKCdpbmRleCcpKTtcblxuXHRcdFx0aWYoZGlyID09ICdsZWZ0Jylcblx0XHRcdHtcblx0XHRcdFx0bmV4dCA9IGluZGV4IC0gMTtcblx0XHRcdFx0aWYobmV4dCA8IDApXG5cdFx0XHRcdHtcblx0XHRcdFx0XHRuZXh0ID0gcGFyZW50LmZpbmQoJy5zdGFuZGFyZC1zbGlkZScpLmxlbmd0aCAtIDE7XG5cdFx0XHRcdH1cblxuXHRcdFx0XHRwYXJlbnQuZmluZCgnLnN0YW5kYXJkLXNsaWRlLnNsaWRlLScgKyBuZXh0KS5jc3Moe2xlZnQ6ICctMTA1JSd9KTtcblxuXHRcdFx0XHRuZXdfb2Zmc2V0ID0gJzEwNSUnO1xuXHRcdFx0fVxuXHRcdFx0ZWxzZVxuXHRcdFx0e1xuXHRcdFx0XHRuZXh0ID0gaW5kZXggKyAxO1xuXG5cdFx0XHRcdGlmKG5leHQgPiBwYXJlbnQuZmluZCgnLnN0YW5kYXJkLXNsaWRlJykubGVuZ3RoIC0gMSlcblx0XHRcdFx0e1xuXHRcdFx0XHRcdG5leHQgPSAwO1xuXHRcdFx0XHR9XG5cblx0XHRcdFx0cGFyZW50LmZpbmQoJy5zdGFuZGFyZC1zbGlkZS5zbGlkZS0nICsgbmV4dCkuY3NzKHtsZWZ0OiAnMTA1JSd9KTtcblxuXHRcdFx0XHRuZXdfb2Zmc2V0ID0gJy0xMDUlJztcblx0XHRcdH1cblxuXHRcdFx0cGFyZW50LmZpbmQoJy5kb3QuYWN0aXZlJykucmVtb3ZlQ2xhc3MoJ2FjdGl2ZScpO1xuXHRcdFx0cGFyZW50LmZpbmQoJy5kb3QtJyArIG5leHQpLmFkZENsYXNzKCdhY3RpdmUnKTtcblxuXHRcdFx0cGFyZW50LmZpbmQoJy5zdGFuZGFyZC1zbGlkZS5zbGlkZS0nICsgbmV4dCkuYW5pbWF0ZSh7bGVmdDogMH0sIDUwMCwgZnVuY3Rpb24oKXtcblx0XHRcdFx0JCh0aGlzKS5hZGRDbGFzcygnYWN0aXZlJyk7XG5cdFx0XHR9KTtcblxuXHRcdFx0cGFyZW50LmZpbmQoJy5zdGFuZGFyZC1zbGlkZS5hY3RpdmUnKS5hbmltYXRlKHtsZWZ0OiBuZXdfb2Zmc2V0fSwgNTAwLCBmdW5jdGlvbigpe1xuXHRcdFx0XHQkKHRoaXMpLnJlbW92ZUNsYXNzKCdhY3RpdmUnKTtcblx0XHRcdFx0cGFyZW50LnJlbW92ZUNsYXNzKCdtb3ZpbmcnKTtcblx0XHRcdH0pO1xuXHRcdH1cblxuXHRcdCQoJy5zdGFuZGFyZC1zbGlkZXNob3cgLmFycm93JykuY2xpY2soZnVuY3Rpb24oKXtcblx0XHRcdHZhciBwYXJlbnQgPSAkKHRoaXMpLnBhcmVudCgpO1xuXG5cdFx0XHRpZihwYXJlbnQuaGFzQ2xhc3MoJ21vdmluZycpKVxuXHRcdFx0e1xuXHRcdFx0XHRyZXR1cm4gZmFsc2U7XG5cdFx0XHR9XG5cblx0XHRcdHBhcmVudC5hZGRDbGFzcygnbW92aW5nIGludGVyYWN0ZWQnKTtcblxuXHRcdFx0dmFyIGRpciA9ICQodGhpcykuaGFzQ2xhc3MoJ2xlZnQnKSA/ICdsZWZ0JyA6ICdyaWdodCc7XG5cblx0XHRcdGJyaW5rZW5iZXJnTW92ZVNsaWRlKHBhcmVudCwgZGlyKTtcblx0XHR9KTtcblxuXHRcdCQoJy5zdGFuZGFyZC1zbGlkZXNob3cgLmRvdCcpLmNsaWNrKGZ1bmN0aW9uKCl7XG5cdFx0XHR2YXIgcGFyZW50ID0gJCh0aGlzKS5wYXJlbnQoKS5wYXJlbnQoKTtcblxuXHRcdFx0aWYocGFyZW50Lmhhc0NsYXNzKCdtb3ZpbmcnKSB8fCAkKHRoaXMpLmhhc0NsYXNzKCdhY3RpdmUnKSlcblx0XHRcdHtcblx0XHRcdFx0cmV0dXJuIGZhbHNlO1xuXHRcdFx0fVxuXG5cdFx0XHRwYXJlbnQuYWRkQ2xhc3MoJ21vdmluZyBpbnRlcmFjdGVkJyk7XG5cblx0XHRcdHZhciBpbmRleCA9IHBhcnNlSW50KCQodGhpcykuZGF0YSgnaW5kZXgnKSk7XG5cdFx0XHR2YXIgY3VyaW5kZXggPSBwYXJzZUludChwYXJlbnQuZmluZCgnLnN0YW5kYXJkLXNsaWRlLmFjdGl2ZScpLmRhdGEoJ2luZGV4JykpO1xuXHRcdFx0dmFyIGRpcjtcblx0XHRcdHZhciBuZXdfb2Zmc2V0O1xuXG5cdFx0XHRpZihpbmRleCA+IGN1cmluZGV4KVxuXHRcdFx0e1xuXHRcdFx0XHRkaXIgPSAncmlnaHQnO1xuXHRcdFx0XHRuZXdfb2Zmc2V0ID0gJy0xMDUlJztcblx0XHRcdFx0cGFyZW50LmZpbmQoJy5zdGFuZGFyZC1zbGlkZS5zbGlkZS0nICsgaW5kZXgpLmNzcyh7bGVmdDogJzEwNSUnfSk7XG5cdFx0XHR9XG5cdFx0XHRlbHNlXG5cdFx0XHR7XG5cdFx0XHRcdGRpciA9ICdsZWZ0Jztcblx0XHRcdFx0bmV3X29mZnNldCA9ICcxMDUlJztcblx0XHRcdFx0cGFyZW50LmZpbmQoJy5zdGFuZGFyZC1zbGlkZS5zbGlkZS0nICsgaW5kZXgpLmNzcyh7bGVmdDogJy0xMDUlJ30pO1xuXHRcdFx0fVxuXG5cdFx0XHRwYXJlbnQuZmluZCgnLmRvdC5hY3RpdmUnKS5yZW1vdmVDbGFzcygnYWN0aXZlJyk7XG5cdFx0XHRwYXJlbnQuZmluZCgnLmRvdC0nICsgaW5kZXgpLmFkZENsYXNzKCdhY3RpdmUnKTtcblxuXHRcdFx0cGFyZW50LmZpbmQoJy5zdGFuZGFyZC1zbGlkZS5zbGlkZS0nICsgaW5kZXgpLmFuaW1hdGUoe2xlZnQ6IDB9LCA1MDAsIGZ1bmN0aW9uKCl7XG5cdFx0XHRcdCQodGhpcykuYWRkQ2xhc3MoJ2FjdGl2ZScpO1xuXHRcdFx0fSk7XG5cblx0XHRcdHBhcmVudC5maW5kKCcuc3RhbmRhcmQtc2xpZGUuYWN0aXZlJykuYW5pbWF0ZSh7bGVmdDogbmV3X29mZnNldH0sIDUwMCwgZnVuY3Rpb24oKXtcblx0XHRcdFx0JCh0aGlzKS5yZW1vdmVDbGFzcygnYWN0aXZlJyk7XG5cdFx0XHRcdHBhcmVudC5yZW1vdmVDbGFzcygnbW92aW5nJyk7XG5cdFx0XHR9KTtcblx0XHR9KTtcblxuXHRcdGlmKCQoJy5zdGFuZGFyZC1zbGlkZXNob3cuYXV0b3BsYXknKS5sZW5ndGgpXG5cdFx0e1xuXHRcdFx0dmFyIHBvcyA9IDA7XG5cdFx0XHR2YXIgcXVldWUgPSBbXTtcblx0XHRcdHZhciBpZDtcblx0XHRcdHZhciBkZWxheSA9IDUwMDA7XG5cblx0XHRcdCQod2luZG93KS5zY3JvbGwoZnVuY3Rpb24oKXtcblx0XHRcdFx0cG9zID0gJCh3aW5kb3cpLnNjcm9sbFRvcCgpO1xuXHRcdFx0XHRcblx0XHRcdFx0JCgnLnN0YW5kYXJkLXNsaWRlc2hvdy5hdXRvcGxheScpLmVhY2goZnVuY3Rpb24oKXtcblx0XHRcdFx0XHRpZihwb3MgPiAkKHRoaXMpLm9mZnNldCgpLnRvcCAtICgkKHdpbmRvdykuaGVpZ2h0KCkgLyAyKSAmJiAhJCh0aGlzKS5oYXNDbGFzcygncGxheWluZycpKVxuXHRcdFx0XHRcdHtcblx0XHRcdFx0XHRcdCQodGhpcykuYWRkQ2xhc3MoJ3BsYXlpbmcnKTtcblx0XHRcdFx0XHRcdGlkID0gJCh0aGlzKS5hdHRyKCdpZCcpO1xuXG5cdFx0XHRcdFx0XHRzZXRUaW1lb3V0KGZ1bmN0aW9uKCl7XG5cdFx0XHRcdFx0XHRcdHF1ZXVlLnB1c2goaWQpO1xuXHRcdFx0XHRcdFx0fSwgZGVsYXkpO1xuXHRcdFx0XHRcdH1cblx0XHRcdFx0fSk7XG5cdFx0XHR9KTtcblxuXHRcdFx0c2V0SW50ZXJ2YWwoZnVuY3Rpb24oKXtcblx0XHRcdFx0aWYocXVldWUubGVuZ3RoKVxuXHRcdFx0XHR7XG5cdFx0XHRcdFx0Zm9yKHZhciBpID0gMDsgaSA8IHF1ZXVlLmxlbmd0aDsgaSArKylcblx0XHRcdFx0XHR7XG5cdFx0XHRcdFx0XHRpZighJCgnIycgKyBxdWV1ZVtpXSkuaGFzQ2xhc3MoJ2ludGVyYWN0ZWQnKSlcblx0XHRcdFx0XHRcdHtcblx0XHRcdFx0XHRcdFx0YnJpbmtlbmJlcmdNb3ZlU2xpZGUoJCgnIycgKyBxdWV1ZVtpXSksICduZXh0Jyk7XG5cdFx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0fVxuXHRcdFx0XHR9XG5cdFx0XHR9LCBkZWxheSk7XG5cdFx0fVxuXHR9XG5cblx0aWYoJCgnLnNwbGl0LWZvcm0nKS5sZW5ndGgpXG5cdHtcblx0XHQkKCcuc3BsaXQtZm9ybSAuYnV0dG9uJykuY2xpY2soZnVuY3Rpb24oZSl7XG5cdFx0XHRlLnByZXZlbnREZWZhdWx0KCk7XG5cblx0XHRcdHZhciBwYXJlbnQgPSAkKHRoaXMpLnBhcmVudCgpLnBhcmVudCgpLnBhcmVudCgpO1xuXG5cdFx0XHRpZihwYXJlbnQuaGFzQ2xhc3MoJ3Byb2Nlc3NpbmcnKSlcblx0XHRcdHtcblx0XHRcdFx0cmV0dXJuIGZhbHNlO1xuXHRcdFx0fVxuXG5cdFx0XHRwYXJlbnQuZmluZCgnLm1lc3NhZ2UnKS5zbGlkZVVwKCk7XG5cdFx0XHRwYXJlbnQuZmluZCgnaW5wdXQnKS5yZW1vdmVDbGFzcygnZXJyb3InKTtcblxuXHRcdFx0dmFyIGZuYW1lID0gcGFyZW50LmZpbmQoJ2lucHV0W25hbWU9XCJmaXJzdC1uYW1lXCJdJykudmFsKCk7XG5cdFx0XHR2YXIgbG5hbWUgPSBwYXJlbnQuZmluZCgnaW5wdXRbbmFtZT1cImxhc3QtbmFtZVwiXScpLnZhbCgpO1xuXHRcdFx0dmFyIGVtYWlsID0gcGFyZW50LmZpbmQoJ2lucHV0W25hbWU9XCJlbWFpbFwiXScpLnZhbCgpO1xuXHRcdFx0dmFyIG9wdGluID0gZmFsc2U7XG5cblx0XHRcdGlmKHBhcmVudC5maW5kKCdpbnB1dFtuYW1lPVwib3B0aW5cIl0nKS5sZW5ndGgpXG5cdFx0XHR7XG5cdFx0XHRcdG9wdGluID0gcGFyZW50LmZpbmQoJ2lucHV0W25hbWU9XCJvcHRpblwiXScpLmlzKCc6Y2hlY2tlZCcpO1xuXHRcdFx0fVxuXG5cdFx0XHR2YXIgZXJyb3JzID0gW107XG5cblx0XHRcdGlmKCQudHJpbShmbmFtZSkgPT0gJycpXG5cdFx0XHR7XG5cdFx0XHRcdGVycm9ycy5wdXNoKCdQbGVhc2UgZW50ZXIgeW91ciBmaXJzdCBuYW1lLicpO1xuXHRcdFx0XHRwYXJlbnQuZmluZCgnaW5wdXRbbmFtZT1cImZpcnN0LW5hbWVcIl0nKS5hZGRDbGFzcygnZXJyb3InKTtcblx0XHRcdH1cblxuXHRcdFx0aWYoJC50cmltKGxuYW1lKSA9PSAnJylcblx0XHRcdHtcblx0XHRcdFx0ZXJyb3JzLnB1c2goJ1BsZWFzZSBlbnRlciB5b3VyIGxhc3QgbmFtZS4nKTtcblx0XHRcdFx0cGFyZW50LmZpbmQoJ2lucHV0W25hbWU9XCJsYXN0LW5hbWVcIl0nKS5hZGRDbGFzcygnZXJyb3InKTtcblx0XHRcdH1cblxuXHRcdFx0aWYoJC50cmltKGVtYWlsKSA9PSAnJylcblx0XHRcdHtcblx0XHRcdFx0ZXJyb3JzLnB1c2goJ1BsZWFzZSBlbnRlciB5b3VyIGVtYWlsIGFkZHJlc3MuJyk7XG5cdFx0XHRcdHBhcmVudC5maW5kKCdpbnB1dFtuYW1lPVwiZW1haWxcIl0nKS5hZGRDbGFzcygnZXJyb3InKTtcblx0XHRcdH1cblxuXHRcdFx0aWYoZXJyb3JzLmxlbmd0aCA9PSAwKVxuXHRcdFx0e1xuXHRcdFx0XHQvL3NlbmQgdG8gc2VydmVyXG5cdFx0XHRcdCQuYWpheCh7XG5cdFx0XHRcdFx0dHlwZTogXCJQT1NUXCIsXG5cdFx0XHRcdFx0ZGF0YVR5cGU6XCJqc29uXCIsXG5cdFx0XHRcdFx0dXJsOiBhamF4dXJsLFxuXHRcdFx0XHRcdGRhdGE6IHsgYWN0aW9uOiBcIm1haWxpbmdfbGlzdF9mb3JtXCIsIGZuYW1lOiBmbmFtZSwgbG5hbWU6bG5hbWUsIGVtYWlsOmVtYWlsLCBvcHRpbjpvcHRpbiB9XG5cdFx0IFx0XHR9KVxuXHRcdCBcdFx0LmRvbmUoZnVuY3Rpb24oIG1zZyApIHtcblx0XHQgXHRcdFx0aWYobXNnLnN0YXR1cyA9PSBcInN1Y2Nlc3NcIilcblx0XHQgXHRcdFx0e1xuXHRcdFx0XHRcdFx0cGFyZW50LmZpbmQoJy5tZXNzYWdlJykucmVtb3ZlQ2xhc3MoJ2Vycm9yJykudGV4dChtc2cubWVzc2FnZSkuc2xpZGVEb3duKCk7XG5cdFx0XHRcdFx0XHRwYXJlbnQuZmluZCgnLmZvcm0tcm93Jykubm90KCcubWVzc2FnZScpLnNsaWRlVXAoKTtcblx0XHQgXHRcdFx0fVxuXHRcdCBcdFx0XHRlbHNlXG5cdFx0IFx0XHRcdHtcblx0XHQgXHRcdFx0XHRwYXJlbnQuZmluZCgnLm1lc3NhZ2UnKS5hZGRDbGFzcygnZXJyb3InKS5odG1sKG1zZy5tZXNzYWdlKS5zbGlkZURvd24oKS5kZWxheSgzMDAwKS5zbGlkZVVwKCk7XG5cdFx0IFx0XHRcdH1cblx0XHQgXHRcdH0pO1xuXG5cblx0XHRcdH1cblx0XHRcdGVsc2Vcblx0XHRcdHtcblx0XHRcdFx0cGFyZW50LmZpbmQoJy5tZXNzYWdlJykuYWRkQ2xhc3MoJ2Vycm9yJykuaHRtbChlcnJvcnMuam9pbignPGJyPicpKS5zbGlkZURvd24oKS5kZWxheSgzMDAwKS5zbGlkZVVwKCk7XG5cdFx0XHR9XG5cdFx0fSk7XG5cblx0XHQkKCcuY2hlY2tib3gtc3R5bGVkJykuY2xpY2soZnVuY3Rpb24oKXtcblx0XHRcdGlmKCQodGhpcykuaGFzQ2xhc3MoJ2FjdGl2ZScpKVxuXHRcdFx0e1xuXHRcdFx0XHQkKHRoaXMpLnJlbW92ZUNsYXNzKCdhY3RpdmUnKTtcblx0XHRcdFx0JCh0aGlzKS5zaWJsaW5ncygnaW5wdXRbdHlwZT1cImNoZWNrYm94XCJdJykucHJvcCgnY2hlY2tlZCcsIGZhbHNlKTtcblx0XHRcdH1cblx0XHRcdGVsc2Vcblx0XHRcdHtcblx0XHRcdFx0JCh0aGlzKS5hZGRDbGFzcygnYWN0aXZlJyk7XG5cdFx0XHRcdCQodGhpcykuc2libGluZ3MoJ2lucHV0W3R5cGU9XCJjaGVja2JveFwiXScpLnByb3AoJ2NoZWNrZWQnLCB0cnVlKTtcblx0XHRcdH1cblx0XHR9KTtcblx0fVxuXG5cdGlmKCQoJy5hY2NvcmRpb24nKS5sZW5ndGgpXG5cdHtcblx0XHQkKCcuYWNjb3JkaW9uLXRpdGxlJykuY2xpY2soZnVuY3Rpb24oKXtcblx0XHRcdGlmKCQodGhpcykuaGFzQ2xhc3MoJ29wZW5lZCcpKVxuXHRcdFx0e1xuXHRcdFx0XHQkKHRoaXMpLnJlbW92ZUNsYXNzKCdvcGVuZWQnKTtcblx0XHRcdFx0JCh0aGlzKS5zaWJsaW5ncygnLmFjY29yZGlvbi10ZXh0Jykuc2xpZGVVcCgpO1xuXHRcdFx0fVxuXHRcdFx0ZWxzZVxuXHRcdFx0e1xuXHRcdFx0XHQkKHRoaXMpLmFkZENsYXNzKCdvcGVuZWQnKTtcblx0XHRcdFx0JCh0aGlzKS5zaWJsaW5ncygnLmFjY29yZGlvbi10ZXh0Jykuc2xpZGVEb3duKCk7XG5cdFx0XHR9XG5cdFx0fSk7XG5cdH1cbn0pOyJdfQ==
